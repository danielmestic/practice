


exports.home = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("home", {
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "SHOP",
				items : items
			});

			db.close();
		});
	});
};

var mdbClient = require('mongodb').MongoClient;
var _         = require("underscore");

function findById(categories, callback, query) {
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');
		collection.find(query).toArray(function(err, items) {
			if (err) return callback(err, null);
			callback(err, items);
			db.close();
		});
	});
}

exports.category = function(req, res) {
	var id = req.params.category;
	console.log(id);
	findById('categories', function(err, categories) {
		console.log(categories);
		var category = _.reduce(categories, function(elem) {
			return elem.categories;
		});
		console.log(category);
		console.log(category.categories);
		res.render("category", {
		_: _,
		title : id,
			items : categories,
			categories: category.categories,
			session : req.session
	});
}, {"id":id});
};





exports.subcategories = function(req, res) {
 var id = req.params.subcategories;
 console.log(id);
	findById('categories', function(err, categories) {
	console.log(categories);
	var category = _.reduce(categories, function(elem) {
		return elem.categories;
	});
	var filtrare = _.filter(category.categories, function(elem){ 
		return elem.id == id ; 
	});
  
	var subcategories = _.reduce(filtrare, function(elem) {
		return elem.categories;
	});
  
  
  res.render("subcategories", {
 _ : _ ,
  title : id,
   items : category.categories,
   categories: category.categories,
   
   subcategories:subcategories.categories,
   session : req.session
 });
}, {"categories.id":id});
};



function findProducts(products, callback, query) {
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
		collection.find(query).toArray(function(err, items) {
			if (err) return callback(err, null);
			callback(err, items);
			db.close();
		});
	});
}

exports.products = function(req, res) {
 var id = req.params.products;
 console.log(id);
 
 
 findProducts('products', function(err, products) {
  var products = _.each(products, function(elem) {
 return elem.products;
 });
  
   res.render("products", {
       _:_,
  title : id,
   items : products,
   products: products,
   session : req.session
 });
}, {"primary_category_id":id});
};



//exports.productsinfo = function(req, res){
//	var id = req.params.productsinfo;
// console.log(id);
//findProducts('products', function(err, products) {
//	var productsinfo = _.each(products, function(elem) {
// return elem.products;
// });
  
 //  res.render("productsinfo", {
 //      _:_,
//  title : id,
//   items : productsinfo,
//   products: productsinfo.products,
//   session : req.session
// });
//}, {"id":id});

//};

